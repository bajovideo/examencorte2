document.addEventListener('DOMContentLoaded', function() {
    const imagenInput = document.getElementById('imagenInput');
    const imagenMostrada = document.getElementById('imagenMostrada');
    
    imagenInput.addEventListener('change', function(event) {
        const file = event.target.files[0];
        
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                imagenMostrada.src = e.target.result;
                imagenMostrada.style.display = 'block';
            };
            reader.readAsDataURL(file);
        } else {
            imagenMostrada.style.display = 'none';
            imagenMostrada.src = '';
        }
    });
});
