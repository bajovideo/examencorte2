// Función para cargar y mostrar la información de las tasas de cambio de países
function mostrarTasasDeCambio() {
    fetch("/js/paises.json")
        .then(response => response.json())
        .then(data => {
            const infoTasasElement = document.getElementById("infoTasas");
            const posicionRange = document.getElementById("posicionRange");
            const posicionValor = document.getElementById("posicionValor");

            // Escuchar cambios en el control de rango
            posicionRange.addEventListener("input", function() {
                const posicion = parseInt(this.value);
                posicionValor.textContent = posicion;
                const paisSeleccionado = data[posicion];

                const tablaHTML = "<table><tr><th>País</th><th>Moneda</th><th>Valor de Cambio</th></tr>" +
                    `<tr>
                        <td>${paisSeleccionado.pais}</td>
                        <td>${paisSeleccionado.moneda}</td>
                        <td>${paisSeleccionado.valor_cambio}</td>
                    </tr>` + "</table>";
                infoTasasElement.innerHTML = tablaHTML;
            });

            // Mostrar la información del país en la posición 0 al cargar la página
            const primerPais = data[0];
            const tablaHTML = "<table><tr><th>País</th><th>Moneda</th><th>Valor de Cambio</th></tr>" +
                `<tr>
                    <td>${primerPais.pais}</td>
                    <td>${primerPais.moneda}</td>
                    <td>${primerPais.valor_cambio}</td>
                </tr>` + "</table>";
            infoTasasElement.innerHTML = tablaHTML;
        })
        .catch(error => {
            console.error("Error al cargar el archivo JSON: " + error);
        });
}

// Llamar a la función para mostrar las tasas de cambio
mostrarTasasDeCambio();