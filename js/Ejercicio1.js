document.getElementById('mostrarTabla').addEventListener('click', function() {
    const tablaSeleccionada = parseInt(document.getElementById('Tabla').value, 10);
    const resultadoDiv = document.getElementById('resultado');
    resultadoDiv.innerHTML = ''; // Limpiamos el contenido anterior

    const tabla = document.createElement('table');

    for (let i = 1; i <= 10; i++) {
        const fila = document.createElement('tr');
        const multiplicador = document.createElement('td');
        const simbolo = document.createElement('td');
        const multiplicando = document.createElement('td');
        const igual = document.createElement('td');
        const resultado = document.createElement('td');

        multiplicador.innerHTML = `<img src="/img/${tablaSeleccionada}.png" alt="${tablaSeleccionada}">`;
        simbolo.innerHTML = '<img src="/img/x.png" alt="x">';
        multiplicando.innerHTML = `<img src="/img/${i}.png" alt="${i}">`;
        igual.innerHTML = '<img src="/img/=.png" alt="=">';

        // Generar imágenes para el resultado
        const resultadoNumero = i * tablaSeleccionada;

        if (resultadoNumero > 10) {
            const resultadoCifras = resultadoNumero.toString().split('');
            resultadoCifras.forEach(cifra => {
                resultado.innerHTML += `<img src="/img/${cifra}.png" alt="${cifra}">`;
            });
        } else {
            resultado.innerHTML = `<img src="/img/${resultadoNumero}.png" alt="${resultadoNumero}">`;
        }

        fila.appendChild(multiplicador);
        fila.appendChild(simbolo);
        fila.appendChild(multiplicando);
        fila.appendChild(igual);

        // Si es un resultado en el que se muestran dos números, mostrarlos en horizontal
        if (resultadoNumero > 10) {
            fila.appendChild(resultado);
        } else {
            fila.appendChild(resultado.firstChild);
        }

        tabla.appendChild(fila);
    }

    resultadoDiv.appendChild(tabla);
});
